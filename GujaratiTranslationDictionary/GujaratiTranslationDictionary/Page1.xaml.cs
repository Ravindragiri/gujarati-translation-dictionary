﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Phone.Controls;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;
using Newtonsoft.Json;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Net.NetworkInformation;

namespace GujaratiTranslationDictionary
{
    public partial class Page1 : PhoneApplicationPage
    {
        # region declare

        class WebRequestState
        {
            public HttpWebRequest Request { get; set; }
            public HttpWebResponse Response { get; set; }
        }

        // Google Translate REST service URL.
        private const string SERVICE_URL = @"http://translate.google.com/translate_a/t?client=j&text={0}&hl=en&sl={1}&tl={2}&multires=1&otf=1&ssel=4&tsel=4&sc=1";

        delegate void WebResponseAvailableEventHandler(object sender, string response);
        event WebResponseAvailableEventHandler WebResponseAvailable;

        // List of all the available languages.
        private List<Language> _languages = new List<Language>();

        #endregion

        #region Constructor

        public Page1()
        {
            InitializeComponent();
            if (InternetIsAvailable())
            {
                LoadLanguages();

                BackKeyPress += Page1_BackKeyPress;
                Loaded += MainPage_Loaded;
                WebResponseAvailable += main_WebResponseAvailableEventHandler;
            }
        }

        void Page1_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            exit();
        }

        private void exit()
        {
            while (NavigationService.BackStack.Any())
                NavigationService.RemoveBackEntry();

            this.IsHitTestVisible = this.IsEnabled = false;
            if (this.ApplicationBar != null)
                foreach (var item in this.ApplicationBar.Buttons.OfType<ApplicationBarIconButton>())
                {
                    item.IsEnabled = false;
                }
        }

        #endregion

        #region Internet Available

        private bool InternetIsAvailable()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                MessageBox.Show("No internet connection is available. Try again later.");

                btnTranslate.IsEnabled = false;
                ((ApplicationBarMenuItem)ApplicationBar.MenuItems[0]).IsEnabled = false;
                
                return false;
            }

            btnTranslate.IsEnabled = true;
            ((ApplicationBarMenuItem)ApplicationBar.MenuItems[0]).IsEnabled = true;
            
            return true;
        }

        #endregion



        bool IsNavigated = false;
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            try
            {
                base.OnNavigatedTo(e);

                string strInput = string.Empty;
                string FromCode = string.Empty;
                string FromName = string.Empty;

                NavigationContext.QueryString.TryGetValue("strInput", out strInput);
                NavigationContext.QueryString.TryGetValue("FromCode", out FromCode);
                NavigationContext.QueryString.TryGetValue("FromName", out FromName);

                if (!string.IsNullOrEmpty(strInput))
                {
                    IsNavigated = true;
                    foreach (var item in lbxFrom.Items.Where(item => ((Language)item).Code == FromCode
                                                                     && ((Language)item).Name == FromName))
                    {
                        lbxFrom.SelectedIndex = lbxFrom.Items.IndexOf(item);
                    }

                    txtInput.Text = strInput;
                    BeginTranslation();
                }
            }
            catch (Exception)
            {

            }
        }

        private void main_WebResponseAvailableEventHandler(object sender, string response)
        {
            response = "{result:" + response + "}";
            string json = string.Empty;

            XDocument xElement = JsonConvert.DeserializeXNode(response);
            for (int i = 0; i < xElement.Element("result").Elements("sentences").Count(); i++)
            {
                json = json + xElement.Element("result").Elements("sentences").ElementAt(i).Element("trans").Value;
            }

            if (!txtInput.Text.Trim().Contains(" "))
            {
                if (response.Contains("terms"))
                {

                    for (int j = 0; j < xElement.Element("result").Elements("dict").Count(); j++)
                    {
                        for (int i = 0; i < xElement.Element("result").Elements("dict").ElementAt(j).Elements("terms").Count(); i++)
                        {
                            if (j == 0 && i == 0)
                                json = xElement.Element("result").Elements("dict").ElementAt(j).Elements("terms").ElementAt(i).Value;
                            else
                                json = json + ", " + xElement.Element("result").Elements("dict").ElementAt(j).Elements("terms").ElementAt(i).Value;
                        }
                    }
                }
            }

            if (json == txtInput.Text)
            {
                MessageBox.Show("No Result Found.");
                image1.Visibility = Visibility.Collapsed;
                customIndeterminateProgressBar.Visibility = Visibility.Collapsed;
            }
            else
            {
                if (json.Length > 258)
                {
                    json = json.Substring(0, 258);
                    json = json.Substring(0, json.LastIndexOf(','));
                }

                string url = @"http://aspspider.ws/ravindragiri%5cDefault.aspx?sImageText=" + json;
                //string url = @"http://localhost:50138/WSRavi/Default2.aspx?sImageText=" + json;

                var movieRequest = (HttpWebRequest)WebRequest.Create(url);

                movieRequest.Method = "POST";
                movieRequest.BeginGetRequestStream(OnRequestReceived, movieRequest);
            }
            
            InsertHistory();
            PreferredLanguageHistory();
        }

        #region Preferred Language

        private void PreferredLanguageHistory()
        {
            using (IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.FileExists("PreferredLanguage.xml"))
                {
                    using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("PreferredLanguage.xml", FileMode.Open, FileAccess.ReadWrite, storage))
                    {
                        XDocument xDocument = XDocument.Load(isoStream);
                        XElement xElement = (from xml2 in xDocument.Descendants("PreferredInfo")
                                             select xml2).FirstOrDefault();

                        //update
                        if (xElement != null)
                        {
                            isoStream.Position = 0;

                            xElement.SetElementValue("FromCode", ((Language)lbxFrom.SelectedItem).Code);
                            xElement.SetElementValue("FromName", ((Language)lbxFrom.SelectedItem).Name);

                            xDocument.Element("PreferredInfos").AddFirst(xElement);
                            xDocument.Save(isoStream);
                        }
                    }
                }
                else
                {
                    createXMLFile();
                }
            }
        }

        private void createXMLFile()
        {
            using (var file = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var stream = file.OpenFile("PreferredLanguage.xml", FileMode.Create))
                {
                    XDocument xDocument = new XDocument();
                    XElement xElement = new XElement("PreferredInfos",
                                            new XElement("PreferredInfo",
                                                new XElement("FromCode", ((Language)lbxFrom.SelectedItem).Code),
                                            new XElement("FromName", ((Language)lbxFrom.SelectedItem).Name)
                                                    )
                                                   );

                    xDocument.Add(xElement);
                    xDocument.Save(stream);
                }
            }
        }

        private void ReadPreferredInfo()
        {
            using (IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.FileExists("PreferredLanguage.xml"))
                {
                    using (IsolatedStorageFileStream location = new IsolatedStorageFileStream("PreferredLanguage.xml", FileMode.Open, FileAccess.Read, storage))
                    {
                        XDocument xDocument = XDocument.Load(location);

                        XElement xElement = (from xml2 in xDocument.Descendants("PreferredInfo")
                                             select xml2).FirstOrDefault();
                        if (xElement != null)
                        {
                            foreach (var item in lbxFrom.Items.Where(item => ((Language)item).Code == xElement.Element("FromCode").Value
                                                                             && ((Language)item).Name == xElement.Element("FromName").Value))
                            {
                                lbxFrom.SelectedIndex = lbxFrom.Items.IndexOf(item);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        private void InsertHistory()
        {
            using (IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.FileExists("HistoryInfos.xml"))
                {
                    using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("HistoryInfos.xml", FileMode.Open, FileAccess.ReadWrite, storage))
                    {
                        XDocument xDocument = XDocument.Load(isoStream);
                        XElement xElement = (from xml2 in xDocument.Descendants("HistoryInfo")
                                             where xml2.Element("InputVal").Value == txtInput.Text
                                             select xml2).FirstOrDefault();

                        #region delete

                        if (xElement != null)
                        {
                            xElement.Remove();
                            isoStream.Dispose();

                            IsolatedStorageFileStream location = new IsolatedStorageFileStream("HistoryInfos.xml", FileMode.Truncate, FileAccess.ReadWrite, storage);
                            StreamWriter file = new StreamWriter(location);
                            xDocument.Save(file);
                            file.Dispose();
                            location.Dispose();
                        }

                        #endregion

                        #region insert

                        isoStream.Dispose();
                        IsolatedStorageFileStream stream = new IsolatedStorageFileStream("HistoryInfos.xml", FileMode.Open, FileAccess.ReadWrite, storage);
                        xElement = new XElement("HistoryInfo",
                                        new XElement("FromCode", ((Language)lbxFrom.SelectedItem).Code),
                                        new XElement("FromName", ((Language)lbxFrom.SelectedItem).Name),
                                        new XElement("InputVal", txtInput.Text)
                                                );
                        stream.Position = 0;
                        xDocument.Element("HistoryInfos").AddFirst(xElement);
                        xDocument.Save(stream);
                        stream.Dispose();

                        #endregion
                    }
                }
                else
                {
                    createXMLFile(((Language)lbxFrom.SelectedItem).Code, ((Language)lbxFrom.SelectedItem).Name, txtInput.Text);
                }
            }
        }

        private void createXMLFile(string FromCode, string FromName, string InputVal)
        {
            using (var file = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var stream = file.OpenFile("HistoryInfos.xml", FileMode.Create))
                {
                    XDocument xDocument = new XDocument();
                    XElement xElement = new XElement("HistoryInfos",
                                            new XElement("HistoryInfo",
                                                new XElement("FromCode", FromCode),
                                                new XElement("FromName", FromName),
                                                new XElement("InputVal", InputVal)
                                                        )
                                                   );

                    xDocument.Add(xElement);
                    xDocument.Save(stream);
                }
            }
        }

        void OnRequestReceived(IAsyncResult result)
        {
            HttpWebRequest req = (HttpWebRequest)result.AsyncState;

            Stream s = req.EndGetRequestStream(result);
            s.Close();

            // do something with stream to write to it..  
            // or, ignore if you hvae nothing to send.  
            req.BeginGetResponse(new AsyncCallback(OnResponseReceived), req);
        }

        void OnResponseReceived(IAsyncResult result)
        {
            HttpWebRequest req = (HttpWebRequest)result.AsyncState;

            HttpWebResponse response = (HttpWebResponse)req.EndGetResponse(result);

            /// explanation:  we have to create the bitmap on the UI thread, using the dispatcher.  
            /// When the request is closed, the stream is closed - so we have to copy the image  
            /// to a temporary stream that gets disposed when the image is no longer needed.  
            /// this Copies one stream to the other in a 1Mb buffer.  
            Stream s = response.GetResponseStream();

            MemoryStream ms = new MemoryStream();
            byte[] bData = new byte[1024 * 1024];
            int iToRead = bData.Length;
            while (iToRead > 0)
            {
                iToRead = s.Read(bData, 0, bData.Length);
                ms.Write(bData, 0, iToRead);
            }
            ms.Seek(0, SeekOrigin.Begin);

            // using an anonymous delegate, set the bitmap image.  
            Dispatcher.BeginInvoke(new Action<Stream>(
                    delegate(Stream stream)
                    {
                        var bi = new BitmapImage();
                        bi.SetSource(stream);

                        var picLibraryImage = new WriteableBitmap(bi);
                        image1.Source = picLibraryImage;

                        customIndeterminateProgressBar.Visibility = Visibility.Collapsed;
                    })
                    ,
                    new object[] { ms });

            // dispose of the response.  
            response.Dispose();
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!IsNavigated)
            {
                ReadPreferredInfo();
            }
        }

        private void HandleWebResponse(IAsyncResult asyncResult)
        {
            WebRequestState state = (WebRequestState)asyncResult.AsyncState;
            HttpWebRequest request = state.Request;

            state.Response = (HttpWebResponse)request.EndGetResponse(asyncResult);

            if (state.Response != null)
            {
                StreamReader reader = new StreamReader(state.Response.GetResponseStream());
                StringBuilder sb = new StringBuilder();
                while (!reader.EndOfStream)
                    sb.Append(reader.ReadLine());
                string text = sb.ToString();
                Dispatcher.BeginInvoke(() =>
                {
                    if (WebResponseAvailable != null)
                    {
                        WebResponseAvailable(this, text);
                    }
                });
            }
        }

        private void btnTranslate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BeginTranslation();
            }
            catch (Exception)
            {

            }
        }

        private void BeginTranslation()
        {
            try
            {
                image1.Source = null;

                ValidationCheck();
                txtInput.Text = txtInput.Text.Trim();

                if (errFrom.Visibility == Visibility.Collapsed && errInput.Visibility == Visibility.Collapsed)
                {
                    Translate();
                }
            }
            catch (Exception)
            {
                customIndeterminateProgressBar.Visibility = Visibility.Collapsed;
            }

        }

        #region Validation

        private void ValidatelbxFrom()
        {
            if (lbxFrom.SelectedItem == null)
            {
                lbxFrom.BorderThickness = new Thickness(1);
                lbxFrom.BorderBrush = new SolidColorBrush(Colors.Red);
                errFrom.Visibility = Visibility.Visible;
            }
            else
            {
                lbxFrom.BorderThickness = new Thickness(0);
                lbxFrom.BorderBrush = new SolidColorBrush(Colors.Black);
                errFrom.Visibility = Visibility.Collapsed;
            }
        }

        private void ValidatetxtInput()
        {
            if (txtInput.Text.Trim() == string.Empty)
            {
                txtInput.BorderBrush = new SolidColorBrush(Colors.Red);
                errInput.Visibility = Visibility.Visible;
            }
            else
            {
                txtInput.BorderBrush = new SolidColorBrush(Colors.Black);
                errInput.Visibility = Visibility.Collapsed;
            }
        }

        private void ValidationCheck()
        {
            ValidatelbxFrom();
            ValidatetxtInput();
        }

        #endregion
        
        private void txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    btnTranslate.Focus(); // Focus on something else to hide the keyboard ;-)
                    BeginTranslation();
                }
            }
            catch (Exception)
            {

            }

        }

        private void LoadLanguages()
        {
            XDocument langDocument = XDocument.Load("Languages.xml");

            _languages = (from language in langDocument.Descendants("language")
                          select new Language
                          {
                              Code = language.Element("code").Value,
                              Name = language.Element("name").Value
                          }).ToList();

            lbxFrom.ItemsSource = _languages;
        }

        private void Translate()
        {
            if (lbxFrom.SelectedItem == null)
            {
                MessageBox.Show("Please, select languages.");
            }
            else
            {
                customIndeterminateProgressBar.Visibility = Visibility.Visible;

                Language from = lbxFrom.SelectedItem as Language;

                string googleTranslateUrl = string.Format(SERVICE_URL, txtInput.Text, from.Code, "gu");
                string UserAgent = "Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US)";

                HttpWebRequest movieRequest = (HttpWebRequest)WebRequest.Create(googleTranslateUrl);
                movieRequest.UserAgent = UserAgent;
                movieRequest.Accept = "application/json; charset=utf-8";
                movieRequest.Method = "POST";
                //movieRequest.ContentType = "text/xml; charset=utf-8";

                WebRequestState state = new WebRequestState();
                state.Request = movieRequest;
                movieRequest.BeginGetResponse(new AsyncCallback(HandleWebResponse), state);
            }
        }

        private void btnHistory_Click(object sender, EventArgs e)
        {
            //transition page code
            NavigationService.Navigate(new Uri("/RecentHistory.xaml", UriKind.Relative));
        }

        private void lbxFrom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                txtInput.Text = string.Empty;

                lbxFrom.BorderThickness = new Thickness(0);
                lbxFrom.BorderBrush = new SolidColorBrush(Colors.Black);
                errFrom.Visibility = Visibility.Collapsed;
                //IsNavigated = true;
            }
            catch (Exception)
            {

            }

        }

        private void errInput_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ValidatetxtInput();
            }
            catch (Exception)
            {

            }
        }

        private void btnFeedback_Click(object sender, EventArgs e)
        {
            try
            {
                EmailComposeTask emailcomposer = new EmailComposeTask
                {
                    To = "rg.goswami@gmail.com",
                    Subject = @"Comment about the ""Gujarati Translation Dictionary"" Application."
                };
                emailcomposer.Show();
            }
            catch (Exception)
            {

            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtInput.Text = string.Empty;
            image1.Source = null;
        }

    }
}