﻿using System;

namespace GujaratiTranslationDictionary
{
    public class Language
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public Language()
        {
            
        }

        public Language(string Code, string Name)
        {
            this.Code = Code;
            this.Name = Name;
        }
    }

    public class History
    {
        public string FromCode { get; set; }
        public string FromName { get; set; }
        public string InputVal { get; set; }
    }
}
