﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using System.Xml.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Shell;

namespace GujaratiTranslationDictionary
{
    public partial class MainPage : PhoneApplicationPage
    {
        // List of all the available languages.
        private List<History> _history = new List<History>();

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            Loaded += MainPage_Loaded;
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            ReadProviderInfo();
        }

        private void ReadProviderInfo()
        {
            using (IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.FileExists("HistoryInfos.xml"))
                {
                    using (IsolatedStorageFileStream location = new IsolatedStorageFileStream("HistoryInfos.xml", FileMode.Open, FileAccess.Read, storage))
                    {
                        XDocument xDocument = XDocument.Load(location);
                        _history = (from history in xDocument.Descendants("HistoryInfo")
                                    select new History
                                    {
                                        FromCode = history.Element("FromCode").Value,
                                        FromName = history.Element("FromName").Value,
                                        InputVal = history.Element("InputVal").Value

                                    }).ToList();

                        lbxFrom.ItemsSource = _history;
                        ((ApplicationBarIconButton)ApplicationBar.Buttons[0]).IsEnabled = _history.Count > 0;
                    }
                }
                else
                {
                    _history = new List<History>();
                    lbxFrom.ItemsSource = _history;
                }
            }
        }

        private void lbxFrom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                removeFromHistory(((History)lbxFrom.SelectedItem).InputVal);

                //transition page code
                NavigationService.Navigate(new Uri(string.Format("/Page1.xaml?strInput={0}&FromCode={1}&FromName={2}",
                    ((History)lbxFrom.SelectedItem).InputVal, ((History)lbxFrom.SelectedItem).FromCode, ((History)lbxFrom.SelectedItem).FromName), UriKind.Relative));
            }
            catch (Exception)
            {

            }

        }

        private void removeFromHistory(string strInput)
        {
            using (IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.FileExists("HistoryInfos.xml"))
                {
                    using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("HistoryInfos.xml", FileMode.Open, FileAccess.ReadWrite, storage))
                    {
                        XDocument xDocument = XDocument.Load(isoStream);
                        XElement xElement = (from xml2 in xDocument.Descendants("HistoryInfo")
                                             where xml2.Element("InputVal").Value == strInput
                                             select xml2).FirstOrDefault();

                        //delete
                        if (xElement != null)
                        {
                            xElement.Remove();
                            isoStream.Dispose();

                            IsolatedStorageFileStream location = new IsolatedStorageFileStream("HistoryInfos.xml", FileMode.Truncate, FileAccess.ReadWrite, storage);
                            StreamWriter file = new StreamWriter(location);
                            xDocument.Save(file);
                            file.Dispose();
                            location.Dispose();
                        }
                    }
                }
            }
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    const string FilePath = "HistoryInfos.xml";
                    if (myIsolatedStorage.FileExists(FilePath))
                    {
                        myIsolatedStorage.DeleteFile(FilePath);
                    }
                }
                ReadProviderInfo();
                ((ApplicationBarIconButton)ApplicationBar.Buttons[0]).IsEnabled = false;
            }
            catch (Exception)
            {

            }
        }



    }
}